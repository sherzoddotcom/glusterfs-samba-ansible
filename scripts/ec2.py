#!/usr/bin/env python
import boto3
import yaml
import json
import datetime
from collections import defaultdict

session = boto3.Session()
ec2 = boto3.client('ec2', region_name = "us-east-1")
instances = ec2.describe_instances(
    Filters=[
        {'Name': 'instance-state-name', 'Values': ['running']},
        {'Name': 'tag:application', 'Values': ['glusterfs-samba']},
        {'Name': 'tag:Type', 'Values': ['node']},
    ]
)

"""YAML structure
---
all:
  vars:
    foo: noop
  hosts:
    ip1:
      foo: bar
    ip4: 

  children:
    webservers:
      vars:
        foo: bar
      hosts:
        ip2:
          foo: bazz
        ip3:



If script is supplied to ansible as is, return this JSON structure

{
    "_meta": {
        "hostvars": {
            "10.0.2.24": {
                "hostname": "node1"
            }
        }
    },
    "all": {
        "children": [
            "ungrouped"
        ]
    },
    "ungrouped": {
        "hosts": [
            "10.0.2.24"
        ]
    }
}


"""



class Inventory:
    _inventory = {
        'all': {
            'hosts': defaultdict(dict)
        }
    }
    
  
    def add_host(self, host):
        if not host in self._inventory['all']['hosts']:
            self._inventory['all']['hosts'][host] = None

    def add_host_var(self, host, var):
        self._inventory['all']['hosts'][host] = json.loads(var)

    def add_host_to_group(self, host, group):
        if 'children' not in self._inventory['all']:
            self._inventory['all']['children'] = defaultdict(lambda: {'hosts': defaultdict(dict)})
        self._inventory['all']['children'][group]['hosts'][host] = self._inventory['all']['hosts'][host]

    def to_json(self):
        return json.dumps(self._inventory)

    def to_yaml(self):
        return yaml.dump(yaml.load(self.to_json(), Loader = yaml.Loader), default_flow_style = False)



inventory = Inventory()

for r in instances['Reservations']:
    for instance in r['Instances']:
        ip = instance['PrivateIpAddress']
        tags = [{i['Key']:i['Value']} for i in instance['Tags'] if 'ansible' in i['Key']]
        inventory.add_host(ip)
        for tag in tags:
            for tag_key, tag_value in tag.items():
                if 'ansible-group' in tag_key and tag_value != "":
                    _ = [inventory.add_host_to_group(ip, group.strip()) for group in tag_value.split(',')]
                if 'ansible-var' in tag_key:
                    inventory.add_host_var(ip, tag_value)
print(inventory.to_yaml())
